﻿# region "Namespaces"  
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Runtime.InteropServices;
using GradeCalculator.Models;

#endregion

namespace GradeCalculator
{
  
    public class Program
    {
        public static GraduatingClass theStudents = new GraduatingClass();

        public static void Main(string[] args)
        {
            //THE APPLICATION STARTS HERE!!!
            Console.WriteLine("Student MarkList Application");

            Console.Write("Enter the Total number  of students: ");

            int numStudents = -1;

            string s = Console.ReadLine();

            numStudents = Convert.ToInt32(s);


            for (int i = 1; i <= numStudents; i++)
            {
                Console.WriteLine("\nEnter " + i.ToString() + " Student Information\n");

                InputRecords();
            }

            ViewRecords();

            char ch = Console.ReadKey().KeyChar;

        }//Main

        public static void InputRecords()
        {

            Console.Write("Student Name: ");

            string name;

            int[] marks = new int[5];

            name = Console.ReadLine();

            for (int i = 1; i <= 5; i++)
            {
                Console.Write("Sub " + i.ToString() + " Grade (0-4): ");

                marks[i - 1] = Convert.ToInt32(Console.ReadLine());
            }//for

            theStudents.AddRecord(name, marks);

        }//Input Records

        public static void ViewRecords()
        {
            Console.WriteLine("_______________________________________________________________");
            Console.WriteLine("SNo Student Name        ENG   MATH   PHY    CHE    BIO     Total");
            Console.WriteLine("_______________________________________________________________");

            for (int i = 0; i < theStudents.MaxStudents; i++)
            {

                Console.Write("{0, -5}", i + 1);

                Console.Write("{0, -19}", theStudents.studList[i].studentname);

                Console.Write("{0, -7}", theStudents.studList[i].studentmarks[0]);

                Console.Write("{0, -7}", theStudents.studList[i].studentmarks[1]);

                Console.Write("{0, -7}", theStudents.studList[i].studentmarks[2]);

                Console.Write("{0, -7}", theStudents.studList[i].studentmarks[3]);

                Console.Write("{0, -7}", theStudents.studList[i].studentmarks[4]);

                Console.Write("{0, -7}", theStudents.studList[i].totalmarks);

                Console.WriteLine();

            }

            Console.WriteLine("_______________________________________________________________");

        }//ViewRecords

    }//Program


}//GradeCalculator

