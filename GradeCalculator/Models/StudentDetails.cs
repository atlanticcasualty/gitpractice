﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GradeCalculator.Models
{
    /// <summary>
    /// Grade information about a specfic student.
    /// </summary>
    public class StudentDetails
    {

        public string studentname;

        public int[] studentmarks = new int[5];

        public int totalmarks;

    }
}
