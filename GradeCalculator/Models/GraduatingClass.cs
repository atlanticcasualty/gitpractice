﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GradeCalculator.Models
{
    /// <summary>
    /// Graduating Class is a collection of students who will be graduating in a certain year.
    /// </summary>
    public class GraduatingClass
    {

            //List of students within this graduating class.
            public List<StudentDetails> studList = new List<StudentDetails>();

           public int MaxStudents;


            /// <summary>
            /// Call this method to add a student to this graduating class.
            /// </summary>
            /// <param name="name"></param>
            /// <param name="marks"></param>
            /// <returns></returns>
            public bool AddRecord(string name, int[] marks)
            {

                StudentDetails stud = new StudentDetails();

                stud.studentname = name;

                stud.studentmarks = marks;

                stud.totalmarks = 0;

                for (int i = 0; i < 5; i++)
                {
                    stud.totalmarks += stud.studentmarks[i];
                }//for

                studList.Add(stud);

                MaxStudents = studList.Count;

                return true;

            }//AddRecord

    }//GraduatingClass
}
